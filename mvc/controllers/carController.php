<?php 
require 'models/carModel.php';
class CarController extends Controller{
    function __construct() {
        parent::__construct();
        $this->view->pageTitle = "Cars";
    }
    public function index(){
        $this->view->render('cars/index');
    }
    function browse(){
        $this->view->pageTitle = "Browse All Cars";
        $carModel = new Car();
        $this->view->list = $carModel->getAll();
        $this->view->render('cars/browse');
        

    }

    function add($params = false){
        if ($params){
            $car = new Car();
            $car->name = $params["name"];
            $car->type = $params["type"];
            $car->color = $params["color"];
            $car->save();
            $this->view->pageTitle = "Car added. Add new Car?";
        } else {
            $this->view->pageTitle = "Add new Car";
        }
        $this->view->render('cars/add');
    }
    function search($params = false){
        if ($params){
            $car = new Car();
            $car->name = $params["name"];
            $car->type = $params["type"];
            $car->color = $params["color"];
            $car->save();
        }
        $this->view->pageTitle = "Car added. Add new Car?";
        $this->view->render('cars/add');
    }
}