<?php 

class IndexController extends Controller{
    function __construct() {
        parent::__construct();
        $this->view->pageTitle = "Homepage";
    }
    public function index(){
        $this->view->render('home/index');
    }
}