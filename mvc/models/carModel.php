<?php
class Car extends Model{
  public $name;
  public $color = 'black';
  public $type = 'SUV';
 
  function __construct() {
    parent::__construct();
  }

  //  public function setColor($color){
  //    $this->color = $color;
  //  }
   
  //  public function settype($type){
  //    $this->type = $type;
  //  }
   
  //  public function getColor(){
  //   return $this->color;
  // }
  
  // public function gettype(){
  //   return $this->type;
  // }
  public function getAll(){
    $this->database->query("select * from cars");
    $html = '';
    foreach($this->database->resultset() as $result){
      $html .= "<tr>";
      foreach($result as $key => $value){
        $html .= "<td>$value</td>";  }
      }
      $html .= "</tr>";
      return $html;
  }
  public function save(){
    $this->database->query('INSERT INTO cars (id, name, type, color) VALUES (NULL, ?, ?, ?);');
    $this->database->bind(1, $this->name, PDO::PARAM_STR);
    $this->database->bind(2, $this->type, PDO::PARAM_STR);
    $this->database->bind(3, $this->color, PDO::PARAM_STR);
    $this->database->execute();
  }
}



// ''cars' ('id', 'name', 'type', 'color') VALUES (NULL, 'Mustang', 'Sport', 'yellow
?>