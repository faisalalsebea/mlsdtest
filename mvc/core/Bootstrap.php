<?php 
class Bootstrap
{
    
    function __construct() {
        if (isset($_GET['url'])) {
            $url = $_GET['url'];
            $url = rtrim($url, '/');
            $url  = explode('/', $url);
        } else {
            $url[0] = "Index";
        }
        $controllerName = $url[0] . "Controller";
        $file = 'controllers/' . $url[0] . 'Controller.php';
        if (file_exists($file)) {
            require $file;   
        } else {
            throw new Exception("|||||||FILE => " . $file . " DOES NOT EXIST |||||"); 
        }

        //CREATE THE CONTROLLER AND PARAMETERS(OPTIONAL) HERE
        $controller = new $controllerName;
        
        

        if (isset($url[2])) {
            
            // $testparams  = explode('?', $url[2]);
            // echo $testparams;
            
//             $params = array_slice($_GET, 1);


//             foreach($_GET as $key => $value){
//                 echo $key . " : " . $value . "<br />\r\n";
//             }
// echo "<hr>";
            $params = array_slice($url, 2);
            $controller->{$url[1]}();
        } else {
            if (isset($url[1])) {
                $params = array_slice($_GET, 1);
                $controller->{$url[1]}($params);

            } else {
                $controller->index();
            }
        }
    }
}
